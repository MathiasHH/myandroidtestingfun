package dk.mathiashh.myandroidtestingfun;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by mhh on 12/22/15.
 */
public class MyFirstActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_first_test);
    }
}
